#!/usr/bin/env python3

import sys
import logging
import json
from json import JSONDecodeError
from packaging.specifiers import SpecifierSet, InvalidSpecifier, LegacySpecifier, Specifier


class BestEffortSpecifier(LegacySpecifier):
    def __init__(self, specifier):
        super().__init__(specifier)
        self.specifiers = {super()}
        try:
            self.specifiers.add(Specifier(specifier))
        except InvalidSpecifier:
            pass

    def contains(self, item, prereleases=None):
        return satisfies(lambda range: range.contains(item), self.specifiers)


# a helper class that use best effort version matching includig PEP386 and PEP
class BestEffortSpecifierSet(SpecifierSet):
    def __init__(self, specifiers=""):
        specifiers = [s.strip() for s in specifiers.split(",") if s.strip()]
        parsed = set()
        for specifier in specifiers:
             parsed.add(BestEffortSpecifier(specifier))
        self._specs = frozenset(parsed)
        self._prereleases = True


def satisfies(func, seq):
    """Return true if f(item) evals to true."""
    for item in seq:
        if func(item):
            return True
    return False


USAGE = 'rangecheck.py <JSON input file>'
logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)

if len(sys.argv) != 2:
    logging.error(USAGE)
    sys.exit(1)

JSON_INPUT_FILE = sys.argv[1]

raw = None

try:
    FILE_HANDLE = open(JSON_INPUT_FILE, 'r')
    raw = json.load(open(JSON_INPUT_FILE, 'r'))
except IOError as io_exception:
    logging.error(str(io_exception))
    sys.exit(1)
except JSONDecodeError as decode_exception:
    logging.error(decode_exception.msg)
    sys.exit(1)

OUT = []
for key in raw:
    version, rang  = key['version'], key['range']
    line = {'version': '',  'range':''}

    if not version:
       line['error'] = 'version not specified'
       continue
    if not rang:
       line['error'] = 'range not specified'
       continue

    try:
       line['version'] = version
       line['range'] = rang
       version_array = rang.replace(' ', '').split('||')
       range_list = list(map(lambda x: BestEffortSpecifierSet(x), version_array))
       line['satisfies'] = satisfies(lambda range: range.contains(version), range_list)
    except InvalidSpecifier as invalid_specifier_exception:
        line['error'] = str(invalid_specifier_exception)

    OUT.append(line)

sys.stdout.write(json.dumps(OUT, indent=4))
