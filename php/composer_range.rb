#!/usr/bin/env ruby
# frozen_string_literal: true

require 'json'
require 'composer/semver'

EXIT_CODE_BAD_ARGS = 2

if ARGV.size != 1
  filename = File.basename($PROGRAM_NAME)
  puts <<~EOS
    SYNOPSIS
      #{filename} - evaluate versions and ranges

    SYNOPSIS
      ./#{filename} <queries>

    DESCRIPTION
      Evaluate a JSON document containing versions and version ranges (queries)
      and generate a JSON document telling if the version is in range.
  EOS
  exit EXIT_CODE_BAD_ARGS
end

def eval_queries(queries)
  queries.map do |query|
    eval_query(query)
  end
end

def eval_query(query)
  version = parse_version(query['version'])
  range = parse_range(query['range'])
  if ::Composer::Semver::Semver.satisfies?(version, range)
   query.merge("satisfies": true)
  else
   query.merge("satisfies": false)
  end
rescue StandardError => e
  query.merge("error": e.to_s)
end

def parse_version(version)
  raise 'version not specified' if version.nil? || version == ''

  version
end

def parse_range(range)
  raise '' if range.nil? || range == ''

  range
end


input_file = ARGV[0]

begin
  json_object = JSON.parse(File.read(input_file))
  if json_object.instance_of?(Array)
    puts JSON.pretty_generate(eval_queries(json_object))
  else
    puts "JSON Array expected"
    exit(1)
  end
rescue StandardError => e
  puts e
  exit(1)
end
